
Imelda.png
size: 2048,2048
format: RGBA8888
filter: Linear,Linear
repeat: none
arm_inner_down
  rotate: false
  xy: 947, 770
  size: 229, 106
  orig: 229, 106
  offset: 0, 0
  index: -1
arm_inner_up_1
  rotate: false
  xy: 578, 376
  size: 90, 204
  orig: 90, 204
  offset: 0, 0
  index: -1
arm_out_down
  rotate: false
  xy: 880, 666
  size: 255, 102
  orig: 255, 102
  offset: 0, 0
  index: -1
arm_out_up_1
  rotate: true
  xy: 274, 169
  size: 108, 225
  orig: 108, 225
  offset: 0, 0
  index: -1
blood_1
  rotate: true
  xy: 245, 512
  size: 13, 22
  orig: 13, 22
  offset: 0, 0
  index: -1
blood_2
  rotate: false
  xy: 1543, 824
  size: 17, 19
  orig: 17, 19
  offset: 0, 0
  index: -1
body_down
  rotate: false
  xy: 605, 805
  size: 340, 310
  orig: 340, 310
  offset: 0, 0
  index: -1
body_up
  rotate: false
  xy: 1244, 1175
  size: 266, 294
  orig: 266, 294
  offset: 0, 0
  index: -1
bozi
  rotate: false
  xy: 501, 170
  size: 94, 117
  orig: 94, 117
  offset: 0, 0
  index: -1
chest_inner
  rotate: false
  xy: 1407, 912
  size: 146, 143
  orig: 146, 143
  offset: 0, 0
  index: -1
chest_out
  rotate: false
  xy: 1381, 758
  size: 160, 152
  orig: 160, 152
  offset: 0, 0
  index: -1
diaodai_inner
  rotate: false
  xy: 426, 288
  size: 40, 237
  orig: 40, 237
  offset: 0, 0
  index: -1
diaodai_out
  rotate: false
  xy: 155, 2
  size: 102, 173
  orig: 102, 173
  offset: 0, 0
  index: -1
erhuan_inner
  rotate: false
  xy: 924, 366
  size: 13, 50
  orig: 13, 50
  offset: 0, 0
  index: -1
erhuan_out
  rotate: false
  xy: 1122, 612
  size: 13, 52
  orig: 13, 52
  offset: 0, 0
  index: -1
eye_1_Angry
  rotate: false
  xy: 1688, 1465
  size: 113, 31
  orig: 113, 31
  offset: 0, 0
  index: -1
eye_1_Battle_Injury
  rotate: false
  xy: 989, 438
  size: 110, 25
  orig: 110, 25
  offset: 0, 0
  index: -1
eye_1_Battle_Mighty
  rotate: false
  xy: 1515, 1458
  size: 113, 30
  orig: 113, 30
  offset: 0, 0
  index: -1
eye_1_Battle_Weak
  rotate: false
  xy: 1688, 1498
  size: 113, 32
  orig: 113, 32
  offset: 0, 0
  index: -1
eye_1_Normal_1
  rotate: true
  xy: 1137, 655
  size: 113, 31
  orig: 113, 31
  offset: 0, 0
  index: -1
eye_1_Normal_2
  rotate: false
  xy: 1512, 1425
  size: 113, 31
  orig: 113, 31
  offset: 0, 0
  index: -1
eye_1_Normal_3
  rotate: true
  xy: 1803, 1415
  size: 115, 30
  orig: 115, 30
  offset: 0, 0
  index: -1
eye_1_Sad
  rotate: false
  xy: 1512, 1360
  size: 113, 30
  orig: 113, 30
  offset: 0, 0
  index: -1
eye_1_Shy
  rotate: false
  xy: 1512, 1392
  size: 113, 31
  orig: 113, 31
  offset: 0, 0
  index: -1
eye_1_Smile
  rotate: false
  xy: 832, 418
  size: 114, 31
  orig: 114, 31
  offset: 0, 0
  index: -1
eye_1_Surprise
  rotate: false
  xy: 259, 2
  size: 113, 34
  orig: 113, 34
  offset: 0, 0
  index: -1
eye_2_Firm
  rotate: false
  xy: 1512, 1328
  size: 113, 30
  orig: 113, 30
  offset: 0, 0
  index: -1
face
  rotate: false
  xy: 947, 878
  size: 219, 237
  orig: 219, 237
  offset: 0, 0
  index: -1
hair_1_1
  rotate: true
  xy: 829, 345
  size: 71, 93
  orig: 71, 93
  offset: 0, 0
  index: -1
hair_1_2
  rotate: true
  xy: 670, 357
  size: 69, 157
  orig: 69, 157
  offset: 0, 0
  index: -1
hair_1_3
  rotate: false
  xy: 468, 374
  size: 108, 206
  orig: 108, 206
  offset: 0, 0
  index: -1
hair_1_4
  rotate: true
  xy: 1168, 1060
  size: 55, 61
  orig: 55, 61
  offset: 0, 0
  index: -1
hair_1_5
  rotate: true
  xy: 880, 579
  size: 85, 240
  orig: 85, 240
  offset: 0, 0
  index: -1
hair_1_6
  rotate: false
  xy: 155, 177
  size: 117, 330
  orig: 117, 330
  offset: 0, 0
  index: -1
hair_1_7
  rotate: false
  xy: 1980, 1599
  size: 63, 221
  orig: 63, 221
  offset: 0, 0
  index: -1
hair_1_8
  rotate: true
  xy: 259, 38
  size: 129, 197
  orig: 129, 197
  offset: 0, 0
  index: -1
hair_1_9
  rotate: true
  xy: 989, 465
  size: 112, 150
  orig: 112, 150
  offset: 0, 0
  index: -1
hand_inner_1
  rotate: true
  xy: 524, 71
  size: 97, 87
  orig: 97, 87
  offset: 0, 0
  index: -1
hand_inner_2
  rotate: false
  xy: 1512, 1254
  size: 66, 72
  orig: 66, 72
  offset: 0, 0
  index: -1
hand_out_1
  rotate: true
  xy: 1178, 779
  size: 135, 201
  orig: 135, 201
  offset: 0, 0
  index: -1
hongyun_1
  rotate: true
  xy: 501, 5
  size: 36, 21
  orig: 36, 21
  offset: 0, 0
  index: -1
hongyun_2
  rotate: true
  xy: 542, 8
  size: 34, 22
  orig: 34, 22
  offset: 0, 0
  index: -1
jianjia_inne_1
  rotate: true
  xy: 374, 2
  size: 34, 39
  orig: 34, 39
  offset: 0, 0
  index: -1
jianjia_inne_2
  rotate: false
  xy: 670, 428
  size: 160, 153
  orig: 160, 153
  offset: 0, 0
  index: -1
jianjia_inne_3
  rotate: false
  xy: 245, 527
  size: 193, 588
  orig: 193, 588
  offset: 0, 0
  index: -1
jianjia_inner_4
  rotate: false
  xy: 1512, 1198
  size: 43, 54
  orig: 43, 54
  offset: 0, 0
  index: -1
jianjia_inner_5
  rotate: false
  xy: 458, 2
  size: 41, 39
  orig: 41, 39
  offset: 0, 0
  index: -1
jianjia_inner_6
  rotate: true
  xy: 458, 43
  size: 124, 64
  orig: 124, 64
  offset: 0, 0
  index: -1
jianjia_inner_7
  rotate: false
  xy: 1244, 1057
  size: 262, 116
  orig: 262, 116
  offset: 0, 0
  index: -1
jianjia_out_1
  rotate: false
  xy: 1580, 1288
  size: 41, 38
  orig: 41, 38
  offset: 0, 0
  index: -1
jianjia_out_2
  rotate: false
  xy: 1835, 1396
  size: 211, 159
  orig: 211, 159
  offset: 0, 0
  index: -1
jianjia_out_3
  rotate: false
  xy: 2, 509
  size: 241, 606
  orig: 241, 606
  offset: 0, 0
  index: -1
jianjia_out_4
  rotate: true
  xy: 468, 289
  size: 83, 106
  orig: 83, 106
  offset: 0, 0
  index: -1
jianjia_out_5
  rotate: true
  xy: 880, 774
  size: 29, 53
  orig: 29, 53
  offset: 0, 0
  index: -1
jianjia_out_6
  rotate: true
  xy: 1980, 1822
  size: 224, 61
  orig: 224, 61
  offset: 0, 0
  index: -1
jianjia_out_7
  rotate: false
  xy: 1168, 916
  size: 237, 139
  orig: 237, 139
  offset: 0, 0
  index: -1
leg_inner
  rotate: false
  xy: 1515, 1490
  size: 171, 556
  orig: 171, 556
  offset: 0, 0
  index: -1
leg_out
  rotate: false
  xy: 1244, 1471
  size: 269, 575
  orig: 269, 575
  offset: 0, 0
  index: -1
mao_inner_1
  rotate: true
  xy: 274, 279
  size: 246, 150
  orig: 246, 150
  offset: 0, 0
  index: -1
mao_inner_2
  rotate: true
  xy: 832, 451
  size: 126, 155
  orig: 126, 155
  offset: 0, 0
  index: -1
mao_out
  rotate: false
  xy: 605, 583
  size: 273, 220
  orig: 273, 220
  offset: 0, 0
  index: -1
mouth_1_Angry
  rotate: true
  xy: 415, 2
  size: 34, 28
  orig: 34, 28
  offset: 0, 0
  index: -1
mouth_1_Battle_Injury
  rotate: false
  xy: 1579, 1209
  size: 35, 15
  orig: 35, 15
  offset: 0, 0
  index: -1
mouth_1_Battle_Mighty
  rotate: true
  xy: 440, 530
  size: 50, 24
  orig: 50, 24
  offset: 0, 0
  index: -1
mouth_1_Battle_Weak
  rotate: false
  xy: 1579, 1226
  size: 34, 21
  orig: 34, 21
  offset: 0, 0
  index: -1
mouth_1_Happy_A
  rotate: false
  xy: 2004, 1557
  size: 42, 16
  orig: 42, 16
  offset: 0, 0
  index: -1
mouth_1_Normal
  rotate: false
  xy: 2004, 1557
  size: 42, 16
  orig: 42, 16
  offset: 0, 0
  index: -1
mouth_1_Sad
  rotate: true
  xy: 1543, 875
  size: 35, 19
  orig: 35, 19
  offset: 0, 0
  index: -1
mouth_1_Shy
  rotate: false
  xy: 1512, 1176
  size: 40, 20
  orig: 40, 20
  offset: 0, 0
  index: -1
mouth_1_Smile
  rotate: false
  xy: 524, 44
  size: 43, 25
  orig: 43, 25
  offset: 0, 0
  index: -1
mouth_1_Surprise
  rotate: true
  xy: 1543, 845
  size: 28, 19
  orig: 28, 19
  offset: 0, 0
  index: -1
mouth_2_Firm_A
  rotate: true
  xy: 524, 2
  size: 40, 16
  orig: 40, 16
  offset: 0, 0
  index: -1
mouth_2_Firm_B
  rotate: false
  xy: 1580, 1268
  size: 40, 18
  orig: 40, 18
  offset: 0, 0
  index: -1
mouth_2_Firm_C
  rotate: true
  xy: 1557, 1212
  size: 40, 20
  orig: 40, 20
  offset: 0, 0
  index: -1
mouth_2_Firm_D
  rotate: true
  xy: 1980, 1557
  size: 40, 22
  orig: 40, 22
  offset: 0, 0
  index: -1
mouth_2_Happy_B
  rotate: false
  xy: 569, 51
  size: 42, 18
  orig: 42, 18
  offset: 0, 0
  index: -1
mouth_2_Happy_C
  rotate: false
  xy: 1630, 1468
  size: 42, 20
  orig: 42, 20
  offset: 0, 0
  index: -1
mouth_2_Happy_D
  rotate: false
  xy: 2004, 1575
  size: 42, 22
  orig: 42, 22
  offset: 0, 0
  index: -1
mouth_2_Unhappy_A
  rotate: false
  xy: 1580, 1249
  size: 37, 17
  orig: 37, 17
  offset: 0, 0
  index: -1
mouth_2_Unhappy_B
  rotate: true
  xy: 1137, 616
  size: 37, 20
  orig: 37, 20
  offset: 0, 0
  index: -1
mouth_2_Unhappy_C
  rotate: false
  xy: 948, 428
  size: 37, 21
  orig: 37, 21
  offset: 0, 0
  index: -1
mouth_2_Unhappy_D
  rotate: false
  xy: 1101, 441
  size: 37, 22
  orig: 37, 22
  offset: 0, 0
  index: -1
pifeng
  rotate: false
  xy: 2, 1117
  size: 1240, 929
  orig: 1240, 929
  offset: 0, 0
  index: -1
yaojia_inner_1
  rotate: false
  xy: 2, 2
  size: 151, 505
  orig: 151, 505
  offset: 0, 0
  index: -1
yaojia_inner_2
  rotate: false
  xy: 1835, 1557
  size: 143, 489
  orig: 143, 489
  offset: 0, 0
  index: -1
yaojia_out_1
  rotate: false
  xy: 440, 582
  size: 163, 533
  orig: 163, 533
  offset: 0, 0
  index: -1
yaojia_out_2
  rotate: false
  xy: 1688, 1532
  size: 145, 514
  orig: 145, 514
  offset: 0, 0
  index: -1
