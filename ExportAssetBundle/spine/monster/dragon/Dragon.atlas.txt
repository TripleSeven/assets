
Dragon.png
size: 512,512
format: RGBA8888
filter: Linear,Linear
repeat: none
Dragon/Dragon01/body
  rotate: false
  xy: 2, 207
  size: 115, 183
  orig: 115, 183
  offset: 0, 0
  index: -1
Dragon/Dragon01/handinner
  rotate: true
  xy: 457, 108
  size: 33, 52
  orig: 33, 52
  offset: 0, 0
  index: -1
Dragon/Dragon01/handinner1
  rotate: false
  xy: 119, 207
  size: 102, 58
  orig: 102, 58
  offset: 0, 0
  index: -1
Dragon/Dragon01/handout
  rotate: false
  xy: 119, 267
  size: 108, 123
  orig: 108, 123
  offset: 0, 0
  index: -1
Dragon/Dragon01/handout1
  rotate: true
  xy: 457, 143
  size: 59, 45
  orig: 59, 45
  offset: 0, 0
  index: -1
Dragon/Dragon01/handout2
  rotate: true
  xy: 235, 2
  size: 82, 54
  orig: 82, 54
  offset: 0, 0
  index: -1
Dragon/Dragon01/head
  rotate: false
  xy: 237, 413
  size: 195, 97
  orig: 195, 97
  offset: 0, 0
  index: -1
Dragon/Dragon01/jaw
  rotate: true
  xy: 419, 113
  size: 64, 36
  orig: 64, 36
  offset: 0, 0
  index: -1
Dragon/Dragon01/jianjia
  rotate: false
  xy: 322, 83
  size: 95, 91
  orig: 95, 91
  offset: 0, 0
  index: -1
Dragon/Dragon01/jianjiainner
  rotate: false
  xy: 448, 204
  size: 62, 33
  orig: 62, 33
  offset: 0, 0
  index: -1
Dragon/Dragon01/leginnerdown
  rotate: true
  xy: 376, 179
  size: 58, 70
  orig: 58, 70
  offset: 0, 0
  index: -1
Dragon/Dragon01/leginnerup
  rotate: false
  xy: 123, 6
  size: 110, 69
  orig: 110, 69
  offset: 0, 0
  index: -1
Dragon/Dragon01/legoutdown
  rotate: true
  xy: 291, 19
  size: 62, 122
  orig: 62, 122
  offset: 0, 0
  index: -1
Dragon/Dragon01/legoutup
  rotate: false
  xy: 123, 77
  size: 80, 128
  orig: 80, 128
  offset: 0, 0
  index: -1
Dragon/Dragon01/neck
  rotate: true
  xy: 434, 414
  size: 96, 72
  orig: 96, 72
  offset: 0, 0
  index: -1
Dragon/Dragon01/thorn
  rotate: false
  xy: 353, 4
  size: 43, 13
  orig: 43, 13
  offset: 0, 0
  index: -1
Dragon/Dragon01/thorn1
  rotate: false
  xy: 291, 4
  size: 60, 13
  orig: 60, 13
  offset: 0, 0
  index: -1
Dragon/Dragon01/thorn2
  rotate: false
  xy: 376, 239
  size: 108, 17
  orig: 108, 17
  offset: 0, 0
  index: -1
Dragon/Dragon01/weiba
  rotate: false
  xy: 2, 392
  size: 233, 118
  orig: 233, 118
  offset: 0, 0
  index: -1
Dragon/Dragon01/winginner
  rotate: false
  xy: 406, 287
  size: 103, 85
  orig: 103, 85
  offset: 0, 0
  index: -1
Dragon/Dragon01/winginner1
  rotate: true
  xy: 392, 258
  size: 27, 118
  orig: 27, 118
  offset: 0, 0
  index: -1
Dragon/Dragon01/winginner2
  rotate: true
  xy: 237, 373
  size: 38, 167
  orig: 38, 167
  offset: 0, 0
  index: -1
Dragon/Dragon01/winginner3
  rotate: true
  xy: 229, 225
  size: 42, 145
  orig: 42, 145
  offset: 0, 0
  index: -1
Dragon/Dragon01/winginner4
  rotate: true
  xy: 223, 176
  size: 47, 138
  orig: 47, 138
  offset: 0, 0
  index: -1
Dragon/Dragon01/wingout
  rotate: false
  xy: 205, 86
  size: 115, 88
  orig: 115, 88
  offset: 0, 0
  index: -1
Dragon/Dragon01/wingout1
  rotate: true
  xy: 229, 269
  size: 38, 161
  orig: 38, 161
  offset: 0, 0
  index: -1
Dragon/Dragon01/wingout2
  rotate: false
  xy: 74, 19
  size: 47, 186
  orig: 47, 186
  offset: 0, 0
  index: -1
Dragon/Dragon01/wingout3
  rotate: true
  xy: 229, 309
  size: 62, 165
  orig: 62, 165
  offset: 0, 0
  index: -1
Dragon/Dragon01/wingout4
  rotate: true
  xy: 2, 16
  size: 189, 70
  orig: 189, 70
  offset: 0, 0
  index: -1
LandShadow
  rotate: false
  xy: 434, 374
  size: 74, 38
  orig: 74, 38
  offset: 0, 0
  index: -1
