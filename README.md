# assets
Extracted assets of the game [Langrisser Mobile](https://langrisser.zlongame.com/main.html).
The extractions scripts can be found [here](https://gitlab.com/langrisser/database).

Units and soldiers are animated via Spine,
so there are no saved images besides icon and paintings (heroes) of them.

[ui/icon](https://gitlab.com/langrisser/assets/tree/master/ExportAssetBundle/ui/icon) has the most interessting images for the normal user.